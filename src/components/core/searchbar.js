import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
//import theme from '../../theme/theme';
//import MenuIcon from '@material-ui/icons/Menu';
//import SearchIcon from '@material-ui/icons/Search';
//import DirectionsIcon from '@material-ui/icons/Directions';
import Chip from '@material-ui/core/Chip';
const useStyles = makeStyles((theme) => ({
  root: {
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
    textAlign: 'center',
    flexGrow: 1,
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
    textAlign: 'center',
  },
}));

export default function CustomizedInputBase() {
  const classes = useStyles();

  return (
    <Grid container justify="center">
      <Grid item xs={3}>
        <h1>Open data portal for anyone!</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do.</p>
        <Paper component="form" className={classes.root}>
          <InputBase
            className={classes.input}
            placeholder="Email address"
            inputProps={{ 'aria-label': 'Email address' }}
          />

          <Button variant="contained" color="primary">
            AUTOMATE
          </Button>
        </Paper>
      </Grid>
    </Grid>
  );
}
