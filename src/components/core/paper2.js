import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

//import { Icon } from '@material-ui/core';
import archive from '../../assets/icons/albums.svg';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

//import { sizing } from '@material-ui/system';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',

    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(40),
      height: theme.spacing(40),
    },
  },

  image: {
    top: '1414px',
    left: '534px',
    width: '53px',
    height: '49px',
    justify: 'center',
    marginTop: '20px',
  },
  title: {
    wrap: 'nowrap',
    // justify:'center',
    textAlign: 'center',
  },
  button: {
    width: '180px',
  height: '32px',
  background: '#FFEFE5 0% 0% no-repeat padding-box',
  borderRadius: '16px',
  opacity: '1',
  textAlign: 'center',
font: 'Regular 16px/24px Roboto',
letterSpacing: '0px',
color:'#FE9D00',
opacity:' 1',
marginLeft:'550px'
  },
  para:{
    marginLeft:'500px'
  },
  line:{
  marginLeft:'600px'
  }
}));

export default function SimplePaper() {
  const classes = useStyles();

  return (
<div>
<Button
  variant="contained"
  size="small"
  className={classes.button}
>
  Our location levels
</Button>
<h2 className={classes.para}>Find insights in the district level</h2>
<h6 className={classes.line}>Datasets organized in district level.</h6>

    <Grid container justify="center" className={classes.root}>

      <Grid container>

        <Paper elevation={3} className={classes.title}>

          <img src={archive} alt="logo" className={classes.image} />
          <h1>District population</h1>
          <p>
            This should be used to tell a story and let your users know more
            about your service. How can you benefit them?
          </p>
        </Paper>
      </Grid>
      <Grid container>

        <Paper elevation={3} className={classes.title}>

          <img src={archive} alt="logo" className={classes.image} />
          <h1>District population</h1>
          <p>
            This should be used to tell a story and let your users know more
            about your service. How can you benefit them?
          </p>
        </Paper>
      </Grid>
      <Grid container>

        <Paper elevation={3} className={classes.title}>

          <img src={archive} alt="logo" className={classes.image} />
          <h1>District population</h1>
          <p>
            This should be used to tell a story and let your users know more
            about your service. How can you benefit them?
          </p>
        </Paper>
      </Grid>

      <Grid container>
        <Paper elevation={3} className={classes.title}>
          <img src={archive} alt="logo" className={classes.image} />
          <h1>Child care</h1>
          <p>
            This should be used to tell a story and let your users know more
            about your service. How can you benefit them?
          </p>
        </Paper>
      </Grid>
      <Grid container>
        <Paper elevation={1} className={classes.title}>
          <img src={archive} alt="logo" className={classes.image} />
          <h1>Decease</h1>
          <p>
            This should be used to tell a story and let your users know more
            about your service. How can you benefit them?
          </p>
        </Paper>
      </Grid>
    </Grid>
    </div>
  );
}
