import React, { Component } from 'react';
import PublicLayout from './button';
import { withStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import SimpleAccordion from './accordion';
import Accordionlayouts from '../../components/layouts/accordionlayout.js';
import Button from '@material-ui/core/Button';

const styles = (theme) => ({
  logo: {
    width: '159px',
    height: '38px',
    border: '0',
    marginBottom: '50px',
    [theme.breakpoints.down('md')]: {
      width: '200px',
      margin: 'auto',
    },
  },
  cart: {
    maxWidth: '430px',
    width: '100%',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
      margin: 'auto',
  },
  questionbox:{
    maxWidth: '430px',
  },
  para:{
    marginLeft:'500px'
  },
  button:{
    textAlign: 'center',
font:' Bold 15px/24px PT Sans',
letterSpacing: '0px',
color: '#FFFFFF',
marginTop:'10px',
marginLeft:'200px',
textTransform: 'uppercase',
opacity: '1',
width: '157px',
height: '45px',
background:' #FE9D00 0% 0% no-repeat padding-box',
borderRadius:'4px',
opacity:' 1',
  }
});

class Login extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div>
      <h2 className={classes.para}>Frequently Asked Questions</h2>
      <h6 className={classes.para}>A Project is Generated for each New Prototype Imported</h6>

      <Grid container spacing={3}>
        <Grid item xs={4} className={classes.cart}>

          <Grid >
            <Accordionlayouts
              title="Question"
              text="A wonderful serenity has taken possession of my entire soul like sweet enjoy my whole heart."
              button="Contact Us"
              href="/hello"
            ></Accordionlayouts>
            <Button className={classes.button}>Contact us</Button>
          </Grid>
          <Grid>
          <Accordionlayouts
            title="About Us"
            text="A wonderful serenity has taken possession of my entire soul like sweet enjoy my whole heart."
            button="About Us"
            href="/hello"
          ></Accordionlayouts>
          <Button className={classes.button}>Contact us</Button>
          </Grid>
        </Grid>
        <Grid item xs={8}>
          <SimpleAccordion />
        </Grid>
      </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Login);
