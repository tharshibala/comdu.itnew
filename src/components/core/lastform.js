import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';

//import { Icon } from '@material-ui/core';
import archive from '../../assets/icons/albums.svg';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

//import { sizing } from '@material-ui/system';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',

    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(40),
      height: theme.spacing(40),
    },
  },

  image: {
    top: '1414px',
    left: '534px',
    width: '53px',
    height: '49px',
    justify: 'center',
    marginTop: '20px',
  },
  title: {
    wrap: 'nowrap',
    // justify:'center',
    textAlign: 'center',
  },
  button: {
    width: '180px',
  height: '32px',
  background: '#FFEFE5 0% 0% no-repeat padding-box',
  borderRadius: '16px',
  opacity: '1',
  textAlign: 'center',
font: 'Regular 16px/24px Roboto',
letterSpacing: '0px',
color:'#FE9D00',
opacity:' 1',
marginLeft:'300px'
  },

  button2: {
    textAlign:' center',
  font: 'Bold 15px/19px PT Sans',
  letterSpacing:' 1.07px',
  color: '#181D33',
  textTransform: 'uppercase',
  opacity: '1',
  width: '150px',
height: '50px',
background: '#FE9D00 0% 0% no-repeat padding-box',
borderRadius:' 4px',
opacity:' 1',
marginLeft:'300px'
  },
  para:{
    marginLeft:'500px'
  },
  line:{
  marginLeft:'600px'
},
input:{

},
form:{
  marginLeft:'-500px',
  width:'500px',
  marginTop:'100px'
}
}));

export default function SimplePaper() {
  const classes = useStyles();

  return (



    <Grid container justify="center" className={classes.root}>
    <Button
      variant="contained"
      size="small"
      className={classes.button}
    >
    Leave a Message
    </Button>
    <h2 className={classes.para}>Tell us about yourself</h2>

    <div>
  <form className={classes.form} >

  <TextField id="outlined-basic" label="Your name" placeholder="Name, Surname"variant="outlined" />
  <TextField id="outlined-basic" label="Your Email Address" placeholder="Email"variant="outlined" />
  <TextField id="outlined-basic" label="Subject" placeholder="UI/UX Design"variant="outlined" />
  <TextField id="outlined-basic" label="Your Phone Number" placeholder="Phone"variant="outlined" />
  <TextField id="outlined-basic" label="How can we help you?" placeholder="Your Message" variant="outlined" />
  <br></br>
  <br></br>
<Button
variant="contained"
  size="small"
  className={classes.button2} >
  Submit
  </Button>
  </form>
</div>



    </Grid>

  );
}
