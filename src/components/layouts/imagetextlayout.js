import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Paper } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
// import InputButton from '../core/InputButton';
// import useMediaQuery from '@material-ui/core/useMediaQuery';
// import {  useTheme } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(1),
      width: theme.spacing(40),
      height: theme.spacing(40),
    },
  },
  image :{top: '1414px',
    marginLeft:'800px',
    width: '500px',
    height: '500px',
    justify:'center',
    marginTop:'20px',
  },
  main :{
    wrap:'nowrap',
    textAlign:'center',
marginLeft:'200px'
  },
  title:{
    textAlign: 'center',
    font:' Bold 20px/30px PT Sans',
    letterSpacing: 0,
    color: '#212529',
    opacity: '1',
    marginTop:'-400px',
    marginRight:'400px'
},
  text:{
width:'500px',
height:'300px',
textAlign: 'Left',

padding:'30px',
marginLeft:'150px'
},
list:{
  marginLeft:'-250px',

}

}));
function InsightsDistrictCard(props) {
  const classes = useStyles(props);
  // const theme = useTheme();
  // const matches = useMediaQuery(theme.breakpoints.up('sm'));
  return (
                <Paper elevation={props.no} className ={classes.main} >
                  <img src={props.image} className ={classes.image}/>
                  <Typography className={classes.title} variant="h5">{props.title}</Typography>
                  <Typography className={classes.text} variant="subtitle1">{props.text}</Typography>
                  <Typography className={classes.list} variant="subtitle1">{props.list}</Typography>
                  <Typography className={classes.list} variant="subtitle1">{props.list}</Typography>

              </Paper>
  );
}
export default InsightsDistrictCard;
