import React from 'react';
import ButtonAppBar from '../components/core/appbar';
import InputBase from '../components/core/searchbar';
import LabelButtons from '../components/core/iconbutton';
import Paper from '../components/core/paper';
import Contact from '../components/core/contact';
import Paper2 from '../components/core/paper2';
import Lastform from '../components/core/lastform';

import Grid from '@material-ui/core/Grid';
import NestedGrid from '../components/core/test';
import Imagetext from '../components/core/imagetext';

//import homeimg from '../assets/images/fornt-Img.png';
function App() {
  return (
    <Grid item xs={12}>
      <ButtonAppBar />
      <InputBase />

      <LabelButtons />
      <Paper />
      <Paper2 />
      <NestedGrid />
      <Imagetext />
<Contact />
<Lastform/>
    </Grid>
  );
}

export default App;
